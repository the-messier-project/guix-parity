(define-module (gnu packages rust-apps)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages crates-io))
  
(define-public rust-pyo3-macros-backend-0.13
  (package
    (name "rust-pyo3-macros-backend")
    (version "0.13.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pyo3-macros-backend" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ybxrwifk8633qgmbrj63gpbbjwly10ar2zdclsz0250rnmhq8qm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/pyo3/pyo3")
    (synopsis "Code generation for PyO3 package")
    (description "Code generation for PyO3 package")
    (license license:asl2.0)))

(define-public rust-pyo3-macros-0.13
  (package
    (name "rust-pyo3-macros")
    (version "0.13.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pyo3-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "13zgvcmzfxl01q2man8aiknd96hvlgkp20mynjjqjrysvcbcafj8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-pyo3-macros-backend"
          ,rust-pyo3-macros-backend-0.13)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/pyo3/pyo3")
    (synopsis "Proc macros for PyO3 package")
    (description "Proc macros for PyO3 package")
    (license license:asl2.0)))

(define-public rust-paste-1
  (package
    (name "rust-paste")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "paste" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hfikh0bds8hqn371l2wf039mp6b5wrmwrwg96jcs6lkjm6mrmn5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis
      "Macros for all your token pasting needs")
    (description
      "Macros for all your token pasting needs")
    (license (list license:expat license:asl2.0))))

(define-public rust-inventory-impl-0.1
  (package
    (name "rust-inventory-impl")
    (version "0.1.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "inventory-impl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lgs8kia3284s34g7078j820cn2viyb6cij86swklwhn93lr9h3m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "")
    (synopsis
      "Implementation of macros for the `inventory` crate")
    (description
      "Implementation of macros for the `inventory` crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-ghost-0.1
  (package
    (name "rust-ghost")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ghost" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yalg3g1g3cz63n3phy7cdhh7p2qd220mrpxy96alwxbpqdwynqs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "")
    (synopsis "Define your own PhantomData")
    (description "Define your own PhantomData")
    (license (list license:expat license:asl2.0))))

(define-public rust-inventory-0.1
  (package
    (name "rust-inventory")
    (version "0.1.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "inventory" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0zzz5sgrkxv1rpim4ihaidzf6jgha919xm4svcrmxjafh3xpw3qg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ctor" ,rust-ctor-0.1)
         ("rust-ghost" ,rust-ghost-0.1)
         ("rust-inventory-impl" ,rust-inventory-impl-0.1))))
    (home-page "")
    (synopsis
      "Typed distributed plugin registration")
    (description
      "Typed distributed plugin registration")
    (license (list license:expat license:asl2.0))))

(define-public rust-pyo3-0.13
  (package
    (name "rust-pyo3")
    (version "0.13.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pyo3" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0k3m7fibz4hp293d2wflfy4jj8w2335xcpjkkdcqmmdcyd667jh0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-ctor" ,rust-ctor-0.1)
         ("rust-hashbrown" ,rust-hashbrown-0.9)
         ("rust-indoc" ,rust-indoc-1)
         ("rust-inventory" ,rust-inventory-0.1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-num-bigint" ,rust-num-bigint-0.3)
         ("rust-num-complex" ,rust-num-complex-0.3)
         ("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-paste" ,rust-paste-1)
         ("rust-pyo3-macros" ,rust-pyo3-macros-0.13)
         ("rust-unindent" ,rust-unindent-0.1))))
    (home-page "https://github.com/pyo3/pyo3")
    (synopsis "Bindings to Python interpreter")
    (description "Bindings to Python interpreter")
    (license license:asl2.0)))

(define-public rust-indenter-0.3
  (package
    (name "rust-indenter")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "indenter" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1j0yjygkiy4wxvdw9jkspf4qfrfjia425p7sw1zjpv2g24pfpmgl"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/yaahc/indenter")
    (synopsis
      "A formatter wrapper that indents the text, designed for error display impls
")
    (description
      "This package provides a formatter wrapper that indents the text, designed for error display impls
")
    (license (list license:expat license:asl2.0))))

(define-public rust-eyre-0.6
  (package
    (name "rust-eyre")
    (version "0.6.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "eyre" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0awxjsn1bwa43kwv1ycgn1qy9zs66syddjcidxfvz1pasp8kj4i2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-indenter" ,rust-indenter-0.3)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-pyo3" ,rust-pyo3-0.13))))
    (home-page "")
    (synopsis
      "Flexible concrete Error Reporting type built on std::error::Error with customizable Reports")
    (description
      "Flexible concrete Error Reporting type built on std::error::Error with customizable Reports")
    (license (list license:expat license:asl2.0))))

(define-public rust-tracing-error-0.1
  (package
    (name "rust-tracing-error")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing-error" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "092y3357af6058mdw7nmr7sysqdka8b4cyaqz940fl2a7nwc1mxl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tracing" ,rust-tracing-0.1)
         ("rust-tracing-subscriber"
          ,rust-tracing-subscriber-0.2))))
    (home-page "https://tokio.rs")
    (synopsis
      "Utilities for enriching errors with `tracing`.
")
    (description
      "Utilities for enriching errors with `tracing`.
")
    (license license:expat)))

(define-public rust-owo-colors-1
  (package
    (name "rust-owo-colors")
    (version "1.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "owo-colors" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0mbs3z0c6p48wh96paa230xf6c6h8nhyyk1d118pybqwx7mv91i3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atty" ,rust-atty-0.2))))
    (home-page "")
    (synopsis
      "Zero-allocation terminal colors that'll make people go owo")
    (description
      "Zero-allocation terminal colors that'll make people go owo")
    (license license:expat)))

(define-public rust-color-spantrace-0.1
  (package
    (name "rust-color-spantrace")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "color-spantrace" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1lb2li71zvpxp80nck98gcqbqm3dnmp43pnlvm52z9x8livy9vmn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-once-cell" ,rust-once-cell-1)
         ("rust-owo-colors" ,rust-owo-colors-1)
         ("rust-tracing-core" ,rust-tracing-core-0.1)
         ("rust-tracing-error" ,rust-tracing-error-0.1))))
    (home-page "")
    (synopsis
      "A pretty printer for tracing_error::SpanTrace based on color-backtrace")
    (description
      "This package provides a pretty printer for tracing_error::SpanTrace based on color-backtrace")
    (license (list license:expat license:asl2.0))))

(define-public rust-object-0.23
  (package
    (name "rust-object")
    (version "0.23.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "object" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1m658q2ci9hk8csbl17zwcg1hpvcxm2sspjb9bzg0kc1cifsp9x9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-compiler-builtins"
          ,rust-compiler-builtins-0.1)
         ("rust-crc32fast" ,rust-crc32fast-1)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-rustc-std-workspace-alloc"
          ,rust-rustc-std-workspace-alloc-1)
         ("rust-rustc-std-workspace-core"
          ,rust-rustc-std-workspace-core-1)
         ("rust-wasmparser" ,rust-wasmparser-0.57))))
    (home-page "")
    (synopsis
      "A unified interface for reading and writing object file formats.")
    (description
      "This package provides a unified interface for reading and writing object file formats.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-adler-0.2
  (package
    (name "rust-adler")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "adler" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0zpdsrfq5bd34941gmrlamnzjfbsx0x586afb7b0jqhr8g1lwapf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-compiler-builtins"
          ,rust-compiler-builtins-0.1)
         ("rust-rustc-std-workspace-core"
          ,rust-rustc-std-workspace-core-1))))
    (home-page "")
    (synopsis
      "A simple clean-room implementation of the Adler-32 checksum")
    (description
      "This package provides a simple clean-room implementation of the Adler-32 checksum")
    (license
      (list license:expat
            license:asl2.0))))

(define-public rust-miniz-oxide-0.4
  (package
    (name "rust-miniz-oxide")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "miniz_oxide" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17d1xp29v5xgh4vahxld14w1c1hgh38qmxpv7i18wy096gn2cb8g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-adler" ,rust-adler-0.2)
         ("rust-autocfg" ,rust-autocfg-1)
         ("rust-compiler-builtins"
          ,rust-compiler-builtins-0.1)
         ("rust-rustc-std-workspace-alloc"
          ,rust-rustc-std-workspace-alloc-1)
         ("rust-rustc-std-workspace-core"
          ,rust-rustc-std-workspace-core-1))))
    (home-page
      "https://github.com/Frommi/miniz_oxide/tree/master/miniz_oxide")
    (synopsis
      "DEFLATE compression and decompression library rewritten in Rust based on miniz")
    (description
      "DEFLATE compression and decompression library rewritten in Rust based on miniz")
    (license
      (list license:expat license:zlib license:asl2.0))))

(define-public rust-wasmparser-0.57
  (package
    (name "rust-wasmparser")
    (version "0.57.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmparser" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19kslk9pv1bcyp85w63dn1adbp13kz7kjha80abnwz27bmbxvz9j"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/bytecodealliance/wasm-tools/tree/main/crates/wasmparser")
    (synopsis
      "A simple event-driven library for parsing WebAssembly binary files.
")
    (description
      "This package provides a simple event-driven library for parsing WebAssembly binary files.
")
    (license (list license:asl2.0))))

(define-public rust-object-0.22
  (package
    (name "rust-object")
    (version "0.22.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "object" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15r383qxxwq08q3a5rfqhp971wd0nixd9ny22xw37jy31qv66fwd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-compiler-builtins"
          ,rust-compiler-builtins-0.1)
         ("rust-crc32fast" ,rust-crc32fast-1)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-rustc-std-workspace-alloc"
          ,rust-rustc-std-workspace-alloc-1)
         ("rust-rustc-std-workspace-core"
          ,rust-rustc-std-workspace-core-1)
         ("rust-wasmparser" ,rust-wasmparser-0.57))))
    (home-page "")
    (synopsis
      "A unified interface for reading and writing object file formats.")
    (description
      "This package provides a unified interface for reading and writing object file formats.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-gimli-0.23
  (package
    (name "rust-gimli")
    (version "0.23.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gimli" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1km657nwcrb0pnv7v0ldhgl9y8s889y2j9jckmws8k2i8bhkyl7n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-compiler-builtins"
          ,rust-compiler-builtins-0.1)
         ("rust-fallible-iterator"
          ,rust-fallible-iterator-0.2)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-rustc-std-workspace-alloc"
          ,rust-rustc-std-workspace-alloc-1)
         ("rust-rustc-std-workspace-core"
          ,rust-rustc-std-workspace-core-1)
         ("rust-stable-deref-trait"
          ,rust-stable-deref-trait-1))))
    (home-page "")
    (synopsis
      "A library for reading and writing the DWARF debugging format.")
    (description
      "This package provides a library for reading and writing the DWARF debugging format.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-cpp-demangle-0.3
  (package
    (name "rust-cpp-demangle")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpp_demangle" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "142knq32bpa2hbp4z0bldjd1x869664l0ff2gdrqx7pryv59x4a4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-afl" ,rust-afl-0.8)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-glob" ,rust-glob-0.3))))
    (home-page "")
    (synopsis "A crate for demangling C++ symbols")
    (description
      "This package provides a crate for demangling C++ symbols")
    (license (list license:asl2.0 license:expat))))

(define-public rust-addr2line-0.14
  (package
    (name "rust-addr2line")
    (version "0.14.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "addr2line" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xs5bsi40zpyxbbylyaysya5h36ykcbg91i82415sxw5wk7q4px5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-compiler-builtins"
          ,rust-compiler-builtins-0.1)
         ("rust-cpp-demangle" ,rust-cpp-demangle-0.3)
         ("rust-fallible-iterator"
          ,rust-fallible-iterator-0.2)
         ("rust-gimli" ,rust-gimli-0.23)
         ("rust-object" ,rust-object-0.22)
         ("rust-rustc-demangle" ,rust-rustc-demangle-0.1)
         ("rust-rustc-std-workspace-alloc"
          ,rust-rustc-std-workspace-alloc-1)
         ("rust-rustc-std-workspace-core"
          ,rust-rustc-std-workspace-core-1)
         ("rust-smallvec" ,rust-smallvec-1))))
    (home-page "")
    (synopsis
      "A cross-platform symbolication library written in Rust, using `gimli`")
    (description
      "This package provides a cross-platform symbolication library written in Rust, using `gimli`")
    (license (list license:asl2.0 license:expat))))

(define-public rust-backtrace-0.3
  (package
    (name "rust-backtrace")
    (version "0.3.56")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "backtrace" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1g716jmrik0fx29va3js4gw8hwk5jlsmvqaa9ryp1c9qyh07c4cx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-addr2line" ,rust-addr2line-0.14)
         ("rust-backtrace-sys" ,rust-backtrace-sys-0.1)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-cpp-demangle" ,rust-cpp-demangle-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-miniz-oxide" ,rust-miniz-oxide-0.4)
         ("rust-object" ,rust-object-0.23)
         ("rust-rustc-demangle" ,rust-rustc-demangle-0.1)
         ("rust-rustc-serialize"
          ,rust-rustc-serialize-0.3)
         ("rust-serde" ,rust-serde-1)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page
      "https://github.com/rust-lang/backtrace-rs")
    (synopsis
      "A library to acquire a stack trace (backtrace) at runtime in a Rust program.
")
    (description
      "This package provides a library to acquire a stack trace (backtrace) at runtime in a Rust program.
")
    (license (list license:expat license:asl2.0))))

(define-public rust-color-eyre-0.5
  (package
    (name "rust-color-eyre")
    (version "0.5.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "color-eyre" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0p1cavn5wvz4ikq6nffiv11phjjv1mwrgx4flbj7d0zxfl406abv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:tests? #false
        #:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-color-spantrace"
          ,rust-color-spantrace-0.1)
         ("rust-eyre" ,rust-eyre-0.6)
         ("rust-indenter" ,rust-indenter-0.3)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-owo-colors" ,rust-owo-colors-1)
         ("rust-tracing-error" ,rust-tracing-error-0.1)
         ("rust-url" ,rust-url-2))
        #:cargo-development-inputs
        (("rust-ansi-parser" ,rust-ansi-parser-0.6)
         ("rust-pretty-assertions"
          ,rust-pretty-assertions-0.6)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-tracing-subscriber"
          ,rust-tracing-subscriber-0.2)
         ("rust-wasm-bindgen-test"
          ,rust-wasm-bindgen-test-0.3))))
    (home-page "")
    (synopsis
      "An error report handler for panics and eyre::Reports for colorful, consistent, and well formatted error reports for all kinds of errors.")
    (description
      "An error report handler for panics and eyre::Reports for colorful, consistent, and well formatted error reports for all kinds of errors.")
    (license (list license:expat license:asl2.0))))


rust-color-eyre-0.5
