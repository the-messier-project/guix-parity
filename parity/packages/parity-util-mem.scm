(define-module (gnu packages rust-apps)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages crates-io))

(define-public rust-memory-units-0.4
  (package
    (name "rust-memory-units")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "memory_units" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hyk1alsdpcw5r33c5yn7pk9h259klfxv4vhzx08y1j7l1di0ll4"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis
      "Safe conversions between units of memory")
    (description
      "Safe conversions between units of memory")
    (license license:mpl2.0)))

(define-public rust-wee-alloc-0.4
  (package
    (name "rust-wee-alloc")
    (version "0.4.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wee_alloc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "13hb19nfqwjwcqlfj46sjz4j49wd7sj6hbjasimcn5xvnakbbcyv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-0.1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-memory-units" ,rust-memory-units-0.4)
         ("rust-spin" ,rust-spin-0.5)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "")
    (synopsis
      "wee_alloc: The Wasm-Enabled, Elfin Allocator")
    (description
      "wee_alloc: The Wasm-Enabled, Elfin Allocator")
    (license license:mpl2.0)))

(define-public rust-parity-util-mem-derive-0.1
  (package
    (name "rust-parity-util-mem-derive")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parity-util-mem-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1chhs58il2awl2blyvg7l39zlpjz5701j5j7474hg2i6dlnc6mzm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-syn" ,rust-syn-1)
         ("rust-synstructure" ,rust-synstructure-0.12))))
    (home-page "")
    (synopsis "Crate for memory reporting")
    (description "Crate for memory reporting")
    (license license:expat)))

(define-public rust-lru-0.6
  (package
    (name "rust-lru")
    (version "0.6.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lru" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0d8ncw3jcwmhz3y5sa0lj7zc3whr91c15gb4nfwd0ynmfcmk9bis"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-hashbrown" ,rust-hashbrown-0.9))))
    (home-page
      "https://github.com/jeromefroe/lru-rs")
    (synopsis "A LRU cache implementation")
    (description
      "This package provides a LRU cache implementation")
    (license license:expat)))

(define-public rust-impl-trait-for-tuples-0.2
  (package
    (name "rust-impl-trait-for-tuples")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "impl-trait-for-tuples" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00s9adswsvk3v0hijzdarhf8gjjhwllv336kp3darvjgyznahrbg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "")
    (synopsis
      "Attribute macro to implement a trait for tuples
")
    (description
      "Attribute macro to implement a trait for tuples
")
    (license (list license:asl2.0 license:expat))))

(define-public rust-scale-info-derive-0.3
  (package
    (name "rust-scale-info-derive")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "scale-info-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pvngc17qps36d6sdrhly3a6ic77sflnsixrhq03a9nc667wx47b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://www.parity.io/")
    (synopsis
      "Derive type info for SCALE encodable types")
    (description
      "Derive type info for SCALE encodable types")
    (license license:asl2.0)))

(define-public rust-scale-info-0.5
  (package
    (name "rust-scale-info")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "scale-info" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "199apsi22894wjq6h0aspjrhkxc17lb2k67brc823w5xd130j61a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-derive-more" ,rust-derive-more-0.99)
         ("rust-parity-scale-codec"
          ,rust-parity-scale-codec-2)
         ("rust-scale-info-derive"
          ,rust-scale-info-derive-0.3)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://www.parity.io/")
    (synopsis
      "Info about SCALE encodable Rust types")
    (description
      "Info about SCALE encodable Rust types")
    (license license:asl2.0)))

(define-public rust-uint-0.9
  (package
    (name "rust-uint")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uint" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0biqsvqs4bgznizks6s12vz4b0sh9715gbc5809wyhc76jlyj7z1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-0.4)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-crunchy" ,rust-crunchy-0.2)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-quickcheck" ,rust-quickcheck-0.9)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-static-assertions"
          ,rust-static-assertions-1))))
    (home-page "http://parity.io")
    (synopsis "Large fixed-size integer arithmetic")
    (description
      "Large fixed-size integer arithmetic")
    (license (list license:expat license:asl2.0))))

(define-public rust-impl-num-traits-0.1
  (package
    (name "rust-impl-num-traits")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "impl-num-traits" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jkva5rw7bz4hm4d757jga3rm8k1h5f4hs22rgl06fsj7awi09lx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-uint" ,rust-uint-0.9))))
    (home-page
      "https://github.com/paritytech/parity-common")
    (synopsis "num-traits implementation for uint.")
    (description
      "num-traits implementation for uint.")
    (license (list license:expat license:asl2.0))))

(define-public rust-primitive-types-0.9
  (package
    (name "rust-primitive-types")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "primitive-types" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1knsd9pjnpzyw0nb5nx7aa7hdyj5z61j94jd18m0l0yb05s96594"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-fixed-hash" ,rust-fixed-hash-0.7)
         ("rust-impl-codec" ,rust-impl-codec-0.5)
         ("rust-impl-num-traits"
          ,rust-impl-num-traits-0.1)
         ("rust-impl-rlp" ,rust-impl-rlp-0.3)
         ("rust-impl-serde" ,rust-impl-serde-0.3)
         ("rust-scale-info" ,rust-scale-info-0.5)
         ("rust-uint" ,rust-uint-0.9))))
    (home-page
      "https://github.com/paritytech/parity-common")
    (synopsis
      "Primitive types shared by Ethereum and Substrate")
    (description
      "Primitive types shared by Ethereum and Substrate")
    (license (list license:expat license:asl2.0))))

(define-public rust-tiny-keccak-2
  (package
    (name "rust-tiny-keccak")
    (version "2.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tiny-keccak" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0dq2x0hjffmixgyf6xv9wgsbcxkd65ld0wrfqmagji8a829kg79c"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crunchy" ,rust-crunchy-0.2))))
    (home-page
      "https://github.com/debris/tiny-keccak")
    (synopsis
      "An implementation of Keccak derived functions.")
    (description
      "An implementation of Keccak derived functions.")
    (license license:cc0)))

(define-public rust-impl-serde-0.3
  (package
    (name "rust-impl-serde")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "impl-serde" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vyz1n3vqp39npnb9fv3if61gql0zxmgcp6fbyjhf5wknv9a8z5l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page
      "https://github.com/paritytech/parity-common")
    (synopsis
      "Serde serialization support for uint and fixed hash.")
    (description
      "Serde serialization support for uint and fixed hash.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rlp-0.5
  (package
    (name "rust-rlp")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rlp" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1a7vpdsqd1ijsm1jixwq1add18vwp16k1iw5p34rcxrygqa6jhz5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-rustc-hex" ,rust-rustc-hex-2))))
    (home-page "")
    (synopsis
      "Recursive-length prefix encoding, decoding, and compression")
    (description
      "Recursive-length prefix encoding, decoding, and compression")
    (license (list license:expat license:asl2.0))))

(define-public rust-impl-rlp-0.3
  (package
    (name "rust-impl-rlp")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "impl-rlp" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "021869d5s47ili9kmhm9y80qpsbf0wwdap14qzfpb84pjbw210pj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rlp" ,rust-rlp-0.5))))
    (home-page
      "https://github.com/paritytech/parity-common")
    (synopsis
      "RLP serialization support for uint and fixed hash.")
    (description
      "RLP serialization support for uint and fixed hash.")
    (license (list license:expat license:asl2.0))))

(define-public rust-parity-scale-codec-derive-2
  (package
    (name "rust-parity-scale-codec-derive")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parity-scale-codec-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "056jlbc4ll0pji68bjmbivklmcy27qcrwmzh2dq6vzf7jx9fcach"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-crate"
          ,rust-proc-macro-crate-0.1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "")
    (synopsis
      "Serialization and deserialization derive macro for Parity SCALE Codec")
    (description
      "Serialization and deserialization derive macro for Parity SCALE Codec")
    (license license:asl2.0)))

(define-public rust-generic-array-0.14
  (package
    (name "rust-generic-array")
    (version "0.14.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "generic-array" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05qqwm9v5asbil9z28wjkmpfvs1c5c99n8n9gwxis3d3r3n6c52h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-typenum" ,rust-typenum-1)
         ("rust-version-check" ,rust-version-check-0.9))))
    (home-page "")
    (synopsis
      "Generic types implementing functionality of arrays")
    (description
      "Generic types implementing functionality of arrays")
    (license license:expat)))

(define-public rust-byte-slice-cast-1
  (package
    (name "rust-byte-slice-cast")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "byte-slice-cast" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "10dw7dliqn9bbqwp31f3nma34gvp7mj66m8ji7sm93580i5bzhb5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis
      "Safely cast bytes slices from/to slices of built-in fundamental numeric types")
    (description
      "Safely cast bytes slices from/to slices of built-in fundamental numeric types")
    (license license:expat)))

(define-public rust-radium-0.6
  (package
    (name "rust-radium")
    (version "0.6.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "radium" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ys4bpp2l701ghdniz90zhkvb5ykmfw2pj0m8pfcbi7bm10qygv4"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/bitvecto-rs/radium")
    (synopsis
      "Portable interfaces for maybe-atomic types")
    (description
      "Portable interfaces for maybe-atomic types")
    (license license:expat)))

(define-public rust-bitvec-0.20
  (package
    (name "rust-bitvec")
    (version "0.20.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bitvec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0k99ghs5nixdyi475pfrhyys2njg573i03jv03bn91r4j3y1y0gm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-funty" ,rust-funty-1)
         ("rust-radium" ,rust-radium-0.6)
         ("rust-serde" ,rust-serde-1)
         ("rust-tap" ,rust-tap-1)
         ("rust-wyz" ,rust-wyz-0.2))))
    (home-page "https://myrrlyn.net/crates/bitvec")
    (synopsis
      "A crate for manipulating memory, bit by bit")
    (description
      "This package provides a crate for manipulating memory, bit by bit")
    (license license:expat)))

(define-public rust-parity-scale-codec-2
  (package
    (name "rust-parity-scale-codec")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parity-scale-codec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1cwjhps57j3id23i05v7visn2ag6jwkacqgf11bzzd8vmvyj7j3m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-0.4)
         ("rust-arrayvec" ,rust-arrayvec-0.5)
         ("rust-bitvec" ,rust-bitvec-0.20)
         ("rust-byte-slice-cast" ,rust-byte-slice-cast-1)
         ("rust-generic-array" ,rust-generic-array-0.14)
         ("rust-parity-scale-codec-derive"
          ,rust-parity-scale-codec-derive-2)
         ("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis
      "SCALE - Simple Concatenating Aggregated Little Endians")
    (description
      "SCALE - Simple Concatenating Aggregated Little Endians")
    (license license:asl2.0)))

(define-public rust-impl-codec-0.5
  (package
    (name "rust-impl-codec")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "impl-codec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vbpm2kmxla0zflaip3vfgj4hxqhcz6fsy7ynxfdvsws6px0w5yz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-parity-scale-codec"
          ,rust-parity-scale-codec-2))))
    (home-page
      "https://github.com/paritytech/parity-common")
    (synopsis
      "Parity Codec serialization support for uint and fixed hash.")
    (description
      "Parity Codec serialization support for uint and fixed hash.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rustc-hex-2
  (package
    (name "rust-rustc-hex")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustc-hex" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mkjy2vbn5kzg67wgngwddlk4snmd8mkjkql2dzrzzfh6ajzcx9y"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/debris/rustc-hex")
    (synopsis
      "rustc-serialize compatible hex conversion traits
")
    (description
      "rustc-serialize compatible hex conversion traits
")
    (license (list license:expat license:asl2.0))))

(define-public rust-derive-arbitrary-0.4
  (package
    (name "rust-derive-arbitrary")
    (version "0.4.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "derive_arbitrary" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rp0z4k0j5ip0bx6dssg97l4q6bakhf6lm5h1lpr3p3kwjsi585i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "")
    (synopsis "Derives arbitrary traits")
    (description "Derives arbitrary traits")
    (license (list license:expat license:asl2.0))))

(define-public rust-arbitrary-0.4
  (package
    (name "rust-arbitrary")
    (version "0.4.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "arbitrary" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0sa55cynafwzvlhyhfpm3vmi2fydj3ipdj5yfbaif7l56cixfmfv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-derive-arbitrary"
          ,rust-derive-arbitrary-0.4))))
    (home-page "")
    (synopsis
      "The trait for generating structured data from unstructured data")
    (description
      "The trait for generating structured data from unstructured data")
    (license (list license:expat license:asl2.0))))

(define-public rust-fixed-hash-0.7
  (package
    (name "rust-fixed-hash")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fixed-hash" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0g29r0zwf09kg70nprn0s444bn6nfsglmiafhl1pm8ajzvbhxkyg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-0.4)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-quickcheck" ,rust-quickcheck-0.9)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-rustc-hex" ,rust-rustc-hex-2)
         ("rust-static-assertions"
          ,rust-static-assertions-1))))
    (home-page
      "https://github.com/paritytech/parity-common")
    (synopsis
      "Macros to define custom fixed-size hash types")
    (description
      "Macros to define custom fixed-size hash types")
    (license (list license:expat license:asl2.0))))

(define-public rust-crunchy-0.2
  (package
    (name "rust-crunchy")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crunchy" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1dx9mypwd5mpfbbajm78xcrg5lirqk7934ik980mmaffg3hdm0bs"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis
      "Crunchy unroller: deterministically unroll constant loops")
    (description
      "Crunchy unroller: deterministically unroll constant loops")
    (license license:expat)))

(define-public rust-ethbloom-0.11
  (package
    (name "rust-ethbloom")
    (version "0.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ethbloom" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wjrrjr2qa4avlqy2b5sdns88skc95bk4b4pjbqd3sppqywn963p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crunchy" ,rust-crunchy-0.2)
         ("rust-fixed-hash" ,rust-fixed-hash-0.7)
         ("rust-impl-codec" ,rust-impl-codec-0.5)
         ("rust-impl-rlp" ,rust-impl-rlp-0.3)
         ("rust-impl-serde" ,rust-impl-serde-0.3)
         ("rust-tiny-keccak" ,rust-tiny-keccak-2))))
    (home-page
      "https://github.com/paritytech/parity-common")
    (synopsis "Ethereum bloom filter")
    (description "Ethereum bloom filter")
    (license (list license:expat license:asl2.0))))

(define-public rust-ethereum-types-0.11
  (package
    (name "rust-ethereum-types")
    (version "0.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ethereum-types" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zdbm9bj60scijc8b2pyiy4i00rvlim6qpbyn7j8b392dbv5sjzn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ethbloom" ,rust-ethbloom-0.11)
         ("rust-fixed-hash" ,rust-fixed-hash-0.7)
         ("rust-impl-codec" ,rust-impl-codec-0.5)
         ("rust-impl-rlp" ,rust-impl-rlp-0.3)
         ("rust-impl-serde" ,rust-impl-serde-0.3)
         ("rust-primitive-types"
          ,rust-primitive-types-0.9)
         ("rust-uint" ,rust-uint-0.9))))
    (home-page
      "https://github.com/paritytech/parity-common")
    (synopsis "Ethereum types")
    (description "Ethereum types")
    (license (list license:expat license:asl2.0))))

(define-public rust-dlmalloc-0.2
  (package
    (name "rust-dlmalloc")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dlmalloc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0m7jy7ja7rnilxmnn215b3vjam434jgbz1s9j5bjvprf1j37099k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-compiler-builtins"
          ,rust-compiler-builtins-0.1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-rustc-std-workspace-core"
          ,rust-rustc-std-workspace-core-1))))
    (home-page
      "https://github.com/alexcrichton/dlmalloc-rs")
    (synopsis
      "A Rust port of the dlmalloc allocator
")
    (description
      "This package provides a Rust port of the dlmalloc allocator
")
    (license (list license:expat license:asl2.0))))

(define-public rust-parity-util-mem-0.9
  (package
    (name "rust-parity-util-mem")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parity-util-mem" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hkfj15f0h946idm9nacbclancxl5sc93qrpz7rgkn32irmqqjk6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-dlmalloc" ,rust-dlmalloc-0.2)
         ("rust-ethereum-types" ,rust-ethereum-types-0.11)
         ("rust-hashbrown" ,rust-hashbrown-0.9)
         ("rust-impl-trait-for-tuples"
          ,rust-impl-trait-for-tuples-0.2)
         ("rust-jemallocator" ,rust-jemallocator-0.3)
         ("rust-libmimalloc-sys"
          ,rust-libmimalloc-sys-0.1)
         ("rust-lru" ,rust-lru-0.6)
         ("rust-mimalloc" ,rust-mimalloc-0.1)
         ("rust-parity-util-mem-derive"
          ,rust-parity-util-mem-derive-0.1)
         ("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-primitive-types"
          ,rust-primitive-types-0.9)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-wee-alloc" ,rust-wee-alloc-0.4)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "")
    (synopsis
      "Collection of memory related utilities")
    (description
      "Collection of memory related utilities")
    (license (list license:expat license:asl2.0))))
