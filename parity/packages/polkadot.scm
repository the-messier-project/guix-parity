(define-module (gnu packages rust-apps)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cargo)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages crates-io))

(define-public polkadot
  (package
    (name "polkadot")
    (version "0.8.27")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/paritytech/polkadot.git")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1zkqmsclhnv14s4mxz7h49kfx8wyi3lyi0dik6jn1fh6w8zr962c"))
       (modules '((guix build utils)))))
    (build-system cargo-build-system)
    (home-page "https://github.com/alacritty/alacritty")
    (synopsis "Implementation of a polkadot node in Rust based on the Substrate framework")
    (description
     "Implementation of a polkadot node in Rust based on the Substrate framework")
    (license license:gpl3+)))

polkadot
